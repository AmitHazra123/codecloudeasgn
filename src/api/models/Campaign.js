const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const campaignSchema = new Schema({
  name: { type: String, required: true },
  note: { type: String },
  status: { type: String, default: "Scheduled", enum: ["Scheduled", "Running", "Completed", "Aborted"] },
  senderName: { type: String },
  senderEmail: { type: String, required: true },
  accessKeyID: { type: String, required: true },
  secretAccessKey: { type: String, required: true },
  region: { type: String, required: true },
  replyEmails: { type: [] },
  subject: { type: String, required: true },
  mailText: { type: String, required: true },
  fname: { type: String },
  mname: { type: String },
  lname: { type: String },
  receiverEmail: { type: String, required: true },
  isMarkedForImmediateSend: { type: Boolean, required: true },
  scheduleDate: { type: Date },
  errorLogs: { type: [] },
  isValid: { type: Boolean, required: true },
  createdOn: { type: Date, required: true, default: new Date() },
  updatedOn: { type: Date, required: true, default: new Date() }
});

module.exports = mongoose.model('campaign', campaignSchema);