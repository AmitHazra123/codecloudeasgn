const mongoose = require('mongoose');
const { MongoCron } = require('mongodb-cron');
const aws = require('aws-sdk');
const Q = require('q');
const utf8 = require('utf8');

// models
const Campaign = require('../models/Campaign');

const {ObjectId} = mongoose.Types;

// cron configuration
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

let collection = {};
db.once('open', () => {
  collection = db.collection('campaignjobs');
  const cron = new MongoCron({
    collection,
    onDocument: doc => {
      runCampaign(doc).then(() => {
        // 
      }).catch(oError => {
        console.log("run campaign error: ", oError);
      });
    },
    onError: err => {
      console.log("scheduling error: ", err);
    }
  });
  cron.start();
});

module.exports.getAllCampaigns = (req, res) => {
  Campaign.find({isValid: true}).then(aCampaigns => {
    res.status(200).send(aCampaigns);
  }).catch(oError => {
    res.status(400).send({error: oError});
  });
}

module.exports.getCampaignById = (req, res) => {
  Campaign.findOne({_id: ObjectId(req.params.id), isValid: true}).then(oCampaign => {
    res.status(200).send(oCampaign);
  }).catch(oError => {
    res.status(400).send({error: oError});
  });
}

module.exports.createCampaign = (req, res) => {
  const oCampaignInfo = req.body;
  if(!oCampaignInfo.name && !oCampaignInfo.senderEmail && !oCampaignInfo.accessKeyID && !oCampaignInfo.secretAccessKey && !oCampaignInfo.region && !oCampaignInfo.subject && !oCampaignInfo.mailText && !oCampaignInfo.receiverEmail && oCampaignInfo.isMarkedForImmediateSend === undefined) {
    return res.status(400).send({
      msg: "The following required fields are required! senderEmail, accessKeyID, secretAccessKey, region, subject, mailText, receiverEmail, isMarkedSendForImmediateSend",
    }); 
  }
  oCampaignInfo.isValid = true;
  oCampaignInfo.createdOn = new Date();
  oCampaignInfo.updatedOn = new Date();
  oCampaignInfo.isMarkedForImmediateSend ? oCampaignInfo.status = "Running" : oCampaignInfo.status = "Scheduled";
  const newCampaign = new Campaign(oCampaignInfo);
  newCampaign.save().then(oCampaign => {
    if(oCampaign.isMarkedForImmediateSend) {
      collection.insertOne({
        senderName: oCampaign.senderName,
        senderEmail: oCampaign.senderEmail,
        accessKeyID: oCampaign.accessKeyID,
        secretAccessKey: oCampaign.secretAccessKey,
        region: oCampaign.region,
        replyEmails: oCampaign.replyEmails,
        subject: oCampaign.subject,
        mailText: oCampaign.mailText,
        fname: oCampaign.fname,
        mname: oCampaign.mname ? oCampaign.mname : "",
        lname: oCampaign.lname,
        receiverEmail: oCampaign.receiverEmail,
        campaignId: oCampaign._id,
        sleepUntil: new Date()
      });
    } else {
      collection.insertOne({
        senderName: oCampaign.senderName,
        senderEmail: oCampaign.senderEmail,
        accessKeyID: oCampaign.accessKeyID,
        secretAccessKey: oCampaign.secretAccessKey,
        region: oCampaign.region,
        replyEmails: oCampaign.replyEmails,
        subject: oCampaign.subject,
        mailText: oCampaign.mailText,
        fname: oCampaign.fname,
        mname: oCampaign.mname ? oCampaign.mname : "",
        lname: oCampaign.lname,
        receiverEmail: oCampaign.receiverEmail,
        campaignId: oCampaign._id,
        sleepUntil: oCampaign.scheduleDate ? new Date(oCampaign.scheduleDate.toString()) : new Date()
      });
    }
    res.status(200).send(oCampaign);
  }).catch(oError => {
    res.status(400).send({error: oError});
  });
}

module.exports.updateCampaignById = (req, res) => {
  const oCampaignInfo = req.body;
  delete oCampaignInfo._id;
  oCampaignInfo.updatedOn = new Date();
  oCampaignInfo.isMarkedForImmediateSend ? oCampaignInfo.status = "Running" : oCampaignInfo.status = "Scheduled";
  Campaign.findOneAndUpdate(
    { _id: ObjectId(req.params.id), isValid: true, status: {$ne: "Completed"} },
    { $set: oCampaignInfo },
    { new: true }
  ).then(oCampaign => {
    if(oCampaign) {
      if(oCampaign.isMarkedForImmediateSend) {
        collection.updateOne(
          { campaignId: oCampaign._id },
          {
            $set: {
              senderName: oCampaign.senderName,
              senderEmail: oCampaign.senderEmail,
              accessKeyID: oCampaign.accessKeyID,
              secretAccessKey: oCampaign.secretAccessKey,
              region: oCampaign.region,
              replyEmails: oCampaign.replyEmails,
              subject: oCampaign.subject,
              mailText: oCampaign.mailText,
              fname: oCampaign.fname,
              mname: oCampaign.mname ? oCampaign.mname : "",
              lname: oCampaign.lname,
              receiverEmail: oCampaign.receiverEmail,
              sleepUntil: new Date()
            }
          }
        );
      } else {
        collection.updateOne(
          { campaignId: oCampaign._id },
          {
            $set: {
              senderName: oCampaign.senderName,
              senderEmail: oCampaign.senderEmail,
              accessKeyID: oCampaign.accessKeyID,
              secretAccessKey: oCampaign.secretAccessKey,
              region: oCampaign.region,
              replyEmails: oCampaign.replyEmails,
              subject: oCampaign.subject,
              mailText: oCampaign.mailText,
              fname: oCampaign.fname,
              mname: oCampaign.mname ? oCampaign.mname : "",
              lname: oCampaign.lname,
              receiverEmail: oCampaign.receiverEmail,
              sleepUntil: oCampaign.scheduleDate ? new Date(oCampaign.scheduleDate.toString()) : new Date()
            }
          }
        );
      }
    } else {
      res.status(400).send({error: 'Campaign not found which is scheduled!'});
    }
    res.status(200).send(oCampaign);
  }).catch(oError => {
    res.status(400).send({error: oError});
  });
}

module.exports.deleteCampaignById = (req, res) => {
  Campaign.findOneAndUpdate({_id: ObjectId(req.params.id), isValid: true}, {$set: {isValid: false}}, {new: true}).then(oCampaign => {
    if(oCampaign) {
      collection.deleteOne({campaignId: ObjectId(req.params.id)});
      res.status(200).send(oCampaign);
    } else {
      res.status(400).send({msg: "Your campaign is already deleted!"});
    }
  }).catch(oError => {
    res.status(400).send({error: oError});
  });
}

function runCampaign(doc) {
  let deferred = Q.defer();

  // configuration of ses object
  aws.config.update({ "accessKeyId": doc.accessKeyID, "secretAccessKey": doc.secretAccessKey, "region": doc.region });
  let ses = new aws.SES({ apiVersion: '2010-12-01' });
  
  let nMaxSendRate = 1;
  let nRequireDelay = 1000;
  let errorLogs = [];

  ses.getSendQuota({}, function(err, oSendQuota) {
    if(!err && oSendQuota) {
      // use 80% of the send rate
      nMaxSendRate = oSendQuota.MaxSendRate >= 1 ? Math.floor(oSendQuota.MaxSendRate * 0.8) : 1;
      nRequireDelay = Math.ceil(nRequireDelay/nMaxSendRate);

      let senderEmail = utf8.encode(doc.senderName?doc.senderName + " <" + doc.senderEmail + ">":doc.senderEmail);

      ses.sendEmail({
        Source: senderEmail,
        Destination: { ToAddresses: [doc.receiverEmail] },
        Message: {
          Subject: {
            Data: doc.subject
          },
          Body: {
            Text: {
              Data: doc.mailText
            }
          }
        },
        ConfigurationSetName: process.env.CONFIGURATION_SET_NAME,
        ReplyToAddresses: doc.replyEmails
      }, function(err) {
        if(err) {
          if(err.code === "Throttling" || err.code === "ThrottlingException") {
            runCampaign(doc);
          } else {
            errorLogs.push(err);
            Campaign.findOneAndUpdate({_id: doc.campaignId, isValid: true}, {$set: {errorLogs: errorLogs, status: "Completed"}}, {new: true}).then(oCampaign => {
              deferred.resolve(oCampaign);
            }).catch(oError => {
              deferred.reject(oError);
            });
          }
        } else {
          Campaign.findOneAndUpdate({_id: doc.campaignId, isValid: true}, {$set: {status: "Completed"}}, {new: true}).then(() => {
            deferred.resolve(doc);
          }).catch(() => {
            deferred.resolve(doc);
          });
        }
      });
    } else {
      errorLogs.push(err);
      Campaign.findOneAndUpdate({_id: doc.campaignId, isValid: true}, {$set: {errorLogs: errorLogs, status: "Aborted"}}, {new: true}).then(oCampaign => {
        deferred.resolve(oCampaign);
      }).catch(oError => {
        deferred.reject(oError);
      });
    }
  });

  return deferred.promise;
}