const router = require("express").Router();

// controllers
const campaignApi = require('../api/controllers/campaign');

router.get('/getAllCampaigns', campaignApi.getAllCampaigns);
router.get('/getCampaignById/:id', campaignApi.getCampaignById);
router.post('/createCampaign', campaignApi.createCampaign);
router.put('/updateCampaignById/:id', campaignApi.updateCampaignById);
router.delete('/deleteCampaignById/:id', campaignApi.deleteCampaignById);

module.exports = router;